FROM centos:6

RUN yum update
RUN yum -y install wget bzip2

RUN wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh && \
    bash Miniconda-latest-Linux-x86_64.sh -b -p /opt/conda && \
    rm Miniconda-latest-Linux-x86_64.sh

ENV PATH=/opt/conda/bin:$PATH
RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh

RUN conda install -y conda-build

# Needed for conda
RUN yum -y install patch make git

# Needed for ROOT
RUN yum -y install glibc-devel libX11-devel libXpm-devel libXft-devel libXext-devel libGLU-devel
