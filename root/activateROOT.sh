export ROOTSYS=$CONDA_PREFIX
export LDFLAGS=-Wl,-rpath,$CONDA_PREFIX/lib,-rpath,$CONDA_PREFIX/lib/root

export CPLUS_INCLUDE_PATH=$CONDA_PREFIX/include/:$CONDA_PREFIX/gcc/include/c++/:$CONDA_PREFIX/gcc/include/c++/x86_64-unknown-linux-gnu/
export C_INCLUDE_PATH=$CONDA_PREFIX/include/:$CONDA_PREFIX/gcc/include/:$CONDA_PREFIX/lib/gcc/x86_64-unknown-linux-gnu/4.8.5/include/

if [ ! -z "$LD_LIBRARY_PATH" ]; then
  echo ""
  echo "WARNING: LD_LIBRARY_PATH is currently set to '$LD_LIBRARY_PATH'";
  echo ""
  echo "Unless this is intentional this probably needs to be unset using: 'unset LD_LIBRARY_PATH'"
  echo ""
fi

if [ ! -z "$PYTHONPATH" ]; then
  echo ""
  echo "WARNING: PYTHONPATH is currently set to '$PYTHONPATH'";
  echo ""
  echo "Unless this is intentional this probably needs to be unset using: 'unset PYTHONPATH'"
  echo ""
fi
