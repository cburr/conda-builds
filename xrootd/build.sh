export LDFLAGS="-L$PREFIX/lib -lncursesw -ltinfow $LDFLAGS"

# Use `sed -i.bak` to support both macOS and linux
# Set the rpath for the python package
sed -i.bak 's/\[xrdlibdir, xrdcllibdir\]/[xrdlibdir, xrdcllibdir], extra_link_args=["-Wl,-rpath,${CMAKE_INSTALL_RPATH}"]/' bindings/python/setup.py.in

mkdir build
cd build

which gcc

cmake \
    -DCMAKE_INSTALL_PREFIX="$PREFIX" \
    -DCMAKE_INSTALL_LIBDIR="$PREFIX/lib" \
    -DOPENSSL_ROOT_DIR="$PREFIX" \
    -DKERBEROS5_ROOT_DIR="$PREFIX" \
    -DPYTHON_EXECUTABLE=$(which python) \
    -DPYTHON_INCLUDE_DIR="${PREFIX}/lib/libpython${PY_VER}m${SHLIB_EXT}" \
    -DPYTHON_LIBRARY="$PREFIX/include/python${PY_VER}m" \
    -DCMAKE_INSTALL_RPATH="$PREFIX/lib" \
    -DCMAKE_SKIP_BUILD_RPATH=ON \
    -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=ON \
    ..

make -j${CPU_COUNT}
make install
